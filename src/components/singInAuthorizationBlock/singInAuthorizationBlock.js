import React, {useEffect, useState} from "react";
import {
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    NavTabs,
    Card,
    Button,
    CardTitle,
    CardText,
    Row,
    Col
} from 'reactstrap';
import classnames from 'classnames';
import facebookImage from "../../image/facebook.png"
import ease from "../../image/ease.png"
import axios from "axios";


function SingInAuthorizationBlock(props) {
    const [activeTab, setActiveTab] = useState(props.numberTab);
    const [userName, setUserName] = useState("")
    const [userPassword, setUserPassword] = useState("")
    const [passwordChange, setPasswordChange] = useState("")
    const [userEmail, setUserEmail] = useState("")
    const [formatPassword, setFormatPassword] = useState(false)
    const [formatPasswordRegistration, setFormatPasswordRegistration] = useState(false)
    const [formatPasswordRegistrationChange, setRegistrationChange] = useState(false)
    const [userInfo, setUserInfo] = useState([])
    const [error, setError] = useState("")


    const register = (e) => {
        e.preventDefault()
        if(userPassword !==passwordChange){
            setError("Пароли не совпадают")
            return setTimeout(() => {
                setError("")
            }, 3000)

        }
        axios.post("http://77.221.147.62:8090/api/v1/users/registration", {
            username: userName,
            password: userPassword,
            email: userEmail
        })
            .then(r => console.log(r))
            .catch(e => {
                setError(e.response.data.message)
                setTimeout(() => {
                    setError("")
                    setUserName(" ")
                    setUserPassword(" ")
                }, 3000)
            })
    }


    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }
    const changePassword = () => {
        if (!formatPassword) {
            setFormatPassword(true)
        } else {
            setFormatPassword(false)
        }
    }

    const logIn = (e) => {
        e.preventDefault();
        axios.post("http://77.221.147.62:8090/api/v1/users/auth", {
            username: userName,
            password: userPassword
        }).then(r => {
            setUserInfo(r.data)
            props.userinfo(r.data,userName)
        })
            .catch(e => {
                setError(e.response.data.message)
                setTimeout(() => {
                    setError("")
                    setUserName(" ")
                    setUserPassword(" ")
                }, 3000)
            })
    }
    return (<>
            <div className="modalTab"
                 onClick={
                     () => props.toggleBlock()}>

            </div>
            <div className="singInBlock">
                {
                    (error.length > 0 ? <div className="errorBlock">
                        <p>{error}</p>
                    </div> : null)
                }

                <p className="welcome"> Добро пожаловать! </p>
                <Nav tabs>
                    <NavItem>
                        <NavLink className={classnames({active: activeTab === '1'})}
                                 onClick={
                                     () => {
                                         toggle('1');
                                     }
                                 }>
                            Войти </NavLink> </NavItem> <NavItem>
                    <NavLink className={classnames({active: activeTab === '2'})}
                             onClick={
                                 () => {
                                     toggle('2');
                                 }
                             }>
                        Зарегистрироваться </NavLink>
                </NavItem>
                </Nav>
                <TabContent activeTab={activeTab}>
                    <TabPane tabId="1"
                             className="sinInTab">
                        <div>
                            <h4> Имя пользователя </h4> <
                            input type="text"
                                  name="name"
                                  onChange={e => setUserName(e.target.value)}/></div>
                        <div>
                            <h4> Пароль </h4>
                            <
                                input name="password"
                                      type={!formatPassword ? "password" : "text"}
                                      onChange={e => setUserPassword(e.target.value)}
                            /> <a href="#"
                                  onClick={changePassword}> < img className="checkPasswordSingIn"
                                                                  src={ease}
                                                                  alt=""/> </a>

                        </div>
                        <div>
                            <p className="singInSistem"> Проблема со входом в систему ? </p>
                            <a href="#"
                               onClick={logIn}> Войти </a>
                            <p className="singInSistem"> Или войти через : </p>
                            <img src={facebookImage} className="faceBookSingIn" alt=""/>
                        </div>

                    </TabPane>
                    <TabPane tabId="2"
                             className="sinInTab">
                        <div>
                            <h4> Имя пользователя </h4> <
                            input type="text"
                                  name="name"
                                  onChange={e => setUserName(e.target.value)}
                        /></div>
                        <div>
                            <h4> Пароль </h4> <
                            input type={!formatPassword ? "password" : "text"}
                                  name="password"
                                  onChange={e => setUserPassword(e.target.value)}
                        /> <a href="#"
                              onClick={changePassword}> < img className="checkPassword"
                                                              src={ease}
                                                              alt=""/> </a></div>
                        <div>
                            <h4> Подтверждение пароля </h4> <
                            input type={!formatPassword ? "password" : "text"}
                                  name="passwordChange"
                                  onChange={e => setPasswordChange(e.target.value)}
                        /> <a href="#"
                              onClick={changePassword}> < img className="checkPasswordDone"
                                                              src={ease}
                                                              alt=""/> </a></div>
                        <div>
                            <h4> Эл.адрес </h4> <
                            input type="email"
                                  name="email"
                                  onChange={e => setUserEmail(e.target.value)}
                        /></div>
                        <div>
                            <p style={{
                                fontSize: "14px",
                                lineHeight: "16px"
                            }}> Нажимая «Зарегистрироваться», вы подтверждаете, что прочли и в полной мере осознаете
                                <span style={{color: "#815AF6"}}> условия обслуживания</span> и положения <span
                                    style={{color: "#815AF6"}}> политики конфиденциальности. </span></p>

                            <a href="#"
                               onClick={register}> Зарегистрироваться </a>

                        </div>

                    </TabPane> </TabContent>
                <div>

                </div>
            </div>
        </>
    )
}

export default SingInAuthorizationBlock