import React, {useState} from "react";
import "../../../style/style.css"
import SingInAuthorizationBlock from "../../singInAuthorizationBlock/singInAuthorizationBlock";


export function FormUserAuth() {
    const [modalBar, setModalBar] = useState(false)
    const [tabNumber, setTabNumber] = useState("")
    const [user, setUser] = useState()


    const toggle = (number) => {
        setTabNumber(number)
        if (!modalBar) {
            setModalBar(true)
        } else {
            setModalBar(false)
        }
    }

    const takeInfoUser = (info, name) => {
        info.name = name
        setUser(info)
        setModalBar(false)
    }

    const singOut = (e) => {
        e.preventDefault()
        setUser("")
    }

    return (
        <div style={{display: "flex", paddingRight: "5px"}}>
            {(modalBar) ?
                <SingInAuthorizationBlock toggleBlock={toggle} numberTab={tabNumber} userinfo={takeInfoUser}/>
                : null}
            {(user) ?
                <div className="userBlock"><p>{user.name}</p> <a href="#" onClick={singOut}>Выйти</a></div>
                :
                <span style={{paddingTop:"8px"}}>
                    <a href="#" className="singInButton" name="singIn" onClick={() => {
                        toggle("1");
                    }}
                    >Войти</a>
                    <a href="#" className="registrationButton" name="registr" onClick={() => {
                        toggle("2");
                    }}>Регистрация</a>
                </span>}

        </div>
    )
}