import React from "react";
import searchImage from"../../../image/search.png"
export function SearchInput(){
    return(
        <div style={{display:"flex"}}>
            <input type="text" placeholder="Поиск" className="inputSearch"/>
            <a href="#" className="buttonSearch"><img src={searchImage}  style={{    verticalAlign: "-webkit-baseline-middle"}} alt=""/></a>

        </div>
    )
}