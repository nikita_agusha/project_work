import React from "react";
import {Label} from "./label/label";
import {SearchInput} from "./searchInput/searchInput";
import {FormUserAuth} from "./formUserAuth/formUserAuth";
import "../../style/style.css"

function UserTab() {
    return (
        <div className="header">
            <Label/>
            <SearchInput/>
            <FormUserAuth/>
        </div>
    )
}

export default UserTab