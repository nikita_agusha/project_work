import PlayListCarousel from "../../components/playListCarousel/playListCarousel";
import PlayListAllVideos from "../../components/playListAllVideos/playListAllVideos";


function PlayList(){
    return(
        <>
            <PlayListCarousel/>
            <PlayListAllVideos/>
        </>
    )

}
export default PlayList