import React from "react";
import UserTab from "../../components/userTab/userTab";
import SideBar from "../SideBar/SideBar";
import PlayList from "../PlayList/PlayList";
import {Col, Row, Container} from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

function Containers() {
    return (
        <div>
            <UserTab/>
                <Row style={{background:"#18171C", margin:"0"}}>
                    <Col xs="2" style={{margin:"0", padding:"0", marginTop:"5px"}}>
                        <SideBar/>
                    </Col>
                    <Col xs="10">
                        <PlayList/>
                    </Col>
                </Row>
        </div>
    )

}

export default Containers