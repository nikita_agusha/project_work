import React, {useState} from "react";
import gameBoy from "../../image/gameboy.png"
import circle from "../../image/circle.png"
import woman from "../../image/woman.png"
import ease from "../../image/ease.png"
import arroyDown from "../../image/arroyDown.png"
import "../../style/style.css"

function SideBar() {
    const [showChannel, setShowChannel] = useState(false)
    const showChannels = () => {
        if (!showChannel) {
            setShowChannel(true)
        } else {
            setShowChannel(false)
        }
    }
    return (
        <div className="sideBar">
            <div style={{display: "inline-block"}}>
                <a href="#" className="gameButton">
                    <span>Игры</span>
                    <img src={gameBoy} className="gameButton_Icon" alt=""/></a>
                <a href="#" className="gameButton">
                    <span>IRL</span>
                    <img src={circle} className="gameButton_Icon_Ball" alt=""/></a>
            </div>
            <div className="sideBar_channel">
                <div style={{
                    display: "flex",
                    justifyContent: "space-around"
                }}>

                    <p className="sideBar_channels_text">Рекомендуемые каналы</p>
                    <a href="#" onClick={() => showChannels()}><img src={arroyDown}
                                                                    style={showChannel?{transform: "rotate(180deg)"}:{transform: "rotate(0)"}} alt=""/></a>
                </div>
                {showChannel ?
                    <div className="sideBar_channels">
                        <div>
                        <span style={{display: "flex"}}>
                            <img src={woman} alt=""/>
                        <p className="sideBar_channels_info">sobaka_8945 <br/> <span>data2</span> </p>
                        </span>
                            <p className="sideBar_channels_info"><span><img src={ease} alt=""/></span>6785</p>
                        </div>
                        <div>
                        <span style={{display: "flex"}}>
                            <img src={woman} alt=""/>
                        <p className="sideBar_channels_info">sobaka_8945 <br/> <span>data2</span> </p>
                        </span>
                            <p className="sideBar_channels_info"><span><img src={ease} alt=""/></span>6785</p>
                        </div>
                        <div>
                        <span style={{display: "flex"}}>
                            <img src={woman} alt=""/>
                        <p className="sideBar_channels_info">sobaka_8945 <br/> <span>data2</span> </p>
                        </span>
                            <p className="sideBar_channels_info"><span><img src={ease} alt=""/></span>6785</p>
                        </div>
                        <div>
                        <span style={{display: "flex"}}>
                            <img src={woman} alt=""/>
                        <p className="sideBar_channels_info">sobaka_8945 <br/> <span>data2</span> </p>
                        </span>
                            <p className="sideBar_channels_info"><span><img src={ease} alt=""/></span>6785</p>
                        </div>
                        <div>
                        <span style={{display: "flex"}}>
                            <img src={woman} alt=""/>
                        <p className="sideBar_channels_info">sobaka_8945 <br/> <span>data2</span> </p>
                        </span>
                            <p className="sideBar_channels_info"><span><img src={ease} alt=""/></span>6785</p>
                        </div>
                        <div>
                        <span style={{display: "flex"}}>
                            <img src={woman} alt=""/>
                        <p className="sideBar_channels_info">sobaka_8945 <br/> <span>data2</span> </p>
                        </span>
                            <p className="sideBar_channels_info"><span><img src={ease} alt=""/></span>6785</p>
                        </div>
                    </div> : null}

            </div>
        </div>
    )
}

export default SideBar