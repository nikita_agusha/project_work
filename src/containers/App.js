import './App.css';
import Containers from "./container/Container";
import "../style/reset.css"

import "../fonts/1/gilroy.css"

function App() {
  return (
    <div className="App" style={{fontFamily: 'Gilroy',  margin:"0 auto", color:"#C1C1C1"}}>
     <Containers/>
    </div>
  );
}

export default App;
